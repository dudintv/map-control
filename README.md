To setup and run in the first time use [User Setup](#markdown-header-user-setup) instruction.

To get a new version use [Update the App](#markdown-header-update-the-app) instruction.

_The "Map Control Page" I'm gonna name as "App"._

# User Setup

Lets define the __main folder__ for the App. For example, it could be "c:\map"

### 1. Paste "dist" folder into __main folder__, it should be "c:\map\dist".

#### First option (complitely manually), I recommend to use this one if the computer doesn't have the Internet:

Just copy the "dist" folder and paste in the __main folder__.

The final full path should be:
```
c:\map\dist
```

_Update the App:_ the same procedure, just manually replace whole dist folder.

#### Second option (by git), if you have the Internet:

Just clone this repositary into ```c:\map``` folder. Before, please make sure the __main folder__ is exist! If not, run command: ```mkdir "c:\map"``` in order to create this folder.

Run it in PowerShell console:
```
cd "c:\map\"; git clone git@bitbucket.org:dudintv/map-control.git; mv ".\map-control\dist\" ".\dist"; rm -r -fo ".\map-control\"
```

If it doesn't work, please update your Git by command: ```git update-git-for-windows```. And make sure by ```git --version``` that you have the version mare than 2.19.

_Update the App:_ you can just pull update from git repository. Run it in PowerShell console:
```
cd "c:\map\"; git clone git@bitbucket.org:dudintv/map-control.git; rm -r -fo ".\dist\"; mv ".\map-control\dist\" ".\dist"; rm -r -fo ".\map-control\"
```

### 2. Install __current__ Nodejs

The official website is <https://nodejs.org> where you can download an installer. And, just install it.

### 3. Test that Nodejs was installed.

Open PowerShell console and run this string:
```
node -v
```
You should see the installed Nodejs version with no any error. The answer should be like this: ```v14.14.0```.

### 4. Install "http-server" package.

Run in PowerShell:
```
npm install --global http-server
```

Full documantation: https://github.com/http-party/http-server#readme

### 5. Install "json-server" package.

You need to run in PowerShell console the command:
```
npm install --global json-server
```

Full documantation: https://github.com/typicode/json-server#readme

#### 5.1 Set default db.json for json-server

Copy "db.json" file from "dist/db.json" to __main folder__. The final path should be ```c:\map\db.json```.

### 6. Run json-server and http-server:

Run them in two different PowerShell windows.

1 window for json-server:
```
cd "c:\map"; json-server --watch db.json
```

2 window for http-server:
```
cd "c:\map\dist"; http-server
```

__NOTE: please, keep PowerShell windows open in order to make json-server and http-server working!__

To stop the servers you need press "ctrl+C" two times.

If it doesn't work and agrue on restriction rules:

1. Check that you have this resctriction:
```
Get-ExecutionPolicy
```
Probably, you will get ```Restricted```.

2. Set a new rule:
```
Set-ExecutionPolicy Unrestricted -Force
```

### 7. Setup VizEngine REST API.

  1. Run VizConfig and open Communication section
  2. Find "REST Webservice" and put ```61000``` as a port
  3. Click "Install"
  4. Don't forget to restart the VizEngine

### 8. Set up App settings.

Find _settings.json_. The full path should be:
```
c:map\dist\settings.json
```

Take a look at the first part of the "settings":
```
{
  "settings": {
    "jsonServerIp": "localhost:3000",
    "graphicHubIp": "127.0.0.1:19398",
    "vizEngineIp": [
      "127.0.0.1:61000",
      "wrong-pc:61000"
    ]
  },
  ...
}
```

#### 8.1. Set up the default address to VizEngine IP.

You can keep _127.0.0.1_ if you installed Nodejs on the same computer where VizEngine is working, or insert you own VizEngine ip-address:
```
vizEngineIp: "127.0.0.1:61000"
```

You can define multiply VizEngines. Don't forget to put "," between IPs.

#### 8.2 Set up the default address to json-server.

Please, insert your ip-address of the computer where json-server is running. The port should be equal as json-server use, i.e. ":3000".
```
jsonServerIp: "168.192.1.1:3000"
```

### Finish. Test the application again, now it should work:
```
http://localhost:8080/
```

# Update the App

__First option,__ if you copied the "dist" folder manually. Repeat the installation step, i.e. copy "dist" folder and paste with override everything in the folder ```c:\map\dist```

__Second option,__ if you have the Internet and used git approach. Just run this command into PowerShell console:
```
cd "c:\map\"; git clone git@bitbucket.org:dudintv/map-control.git; rm -r -fo ".\dist\"; mv ".\map-control\dist\" ".\dist"; rm -r -fo ".\map-control\"
```

# Update settings.json

It should be a valid JSON file. If you are not sure, please check it here: https://codebeautify.org/jsonvalidator

__NOTE:__ If you change settings.json you have to hard-refresh (Shift+Ctrl+R) browser page in order to load apply new settings!

If it doesn't work, try to restart http-server.

---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------

# Developer usage

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
