export default {
  state: {
    vizGraphicHub: {
      ip: '127.0.0.1:19398',
      status: '',
    },
    vizGhUser: 'Admin',
    vizGhPassword: 'VizDb',
    defaultPath: '',
    folders: [],
    currentFolderIndex: 0,
    currentFolderPath: '',
    files: [],
    selectedFile: {},
    defaultFolder: {
      parentUUID: undefined,
      path: '',
      UUID: '',
      level: 0,
      title: '',
      category: '',
      folders: [],
      files: [],
      isInfoLoaded: false,
      isExpanded: false,
    }
  },
  getters: {
    folders (state) {
      return state.folders
    },
    files (state) {
      return state.files
    }
  },
  mutations: {
    setFolders (state, newFolders) {
      state.folders = newFolders
    },
    collapseFolder (state, folderUUID) {
      const folderIndex = state.folders.findIndex(folder => folder.UUID === folderUUID)
      let childrenCount = 0
      for (let index = folderIndex+1; index < state.folders.length; index++) {
        if (state.folders[index].level > state.folders[folderIndex].level) {
          childrenCount++
        } else {
          break
        }
      }
      state.folders.splice(folderIndex+1, childrenCount)
      state.folders[folderIndex].isExpanded = false
    },
    expandFolder (state, folderUUID) {
      const folderIndex = state.folders.findIndex(folder => folder.UUID === folderUUID)
      const folder = state.folders[folderIndex]
      state.folders.splice(folderIndex+1, 0, ...folder.folders)
      folder.isExpanded = true
    },
    updateFolderInfo(state, { folderUUID, folderInfo }) {
      const folderIndex = state.folders.findIndex(folder => folder.UUID === folderUUID)
      const updatedFolder = { 
        ...state.folders[folderIndex],
        ...folderInfo,
        isInfoLoaded: true,
      }
      state.folders.splice(folderIndex, 1, updatedFolder)
    },
    setCurrentFolderIndex (state, newCurrentFolderIndex) {
      state.currentFolderIndex = newCurrentFolderIndex
    },
    setCurrentFolderPath (state, newCurrentFolderPath) {
      state.currentFolderPath = newCurrentFolderPath
    },
    setFiles (state, newFiles) {
      state.files = newFiles
    },
    setSelectedFile (state, newSelectedFile) {
      state.selectedFile = newSelectedFile
    }
  },
  actions: {
    async getXml ({ state }, path) {
      return new Promise((resolve, reject) => {
        fetch(path, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/xml',
            'Authorization': 'Basic ' + btoa(state.vizGhUser + ":" + state.vizGhPassword)
          }
        })
        .then(response => response.text())
        .then(xmlText => (new window.DOMParser()).parseFromString(xmlText, 'application/xml'))
        .then(xmlData => {
          if(!xmlData || xmlData.documentElement?.firstChild?.firstChild?.tagName === 'parsererror'){
            console.error('ERROR =', xmlData.documentElement.firstChild.firstChild)
            reject(xmlData.documentElement.firstChild.firstChild)
          }
          state.vizGraphicHub.status = 'ok'
          resolve(xmlData)
        })
        .catch(error => {
          console.error('ERROR =', error)
          state.vizGraphicHub.status = 'error'
          reject(error)
        })
      })
    },
    async getFolderInfo ({ state, dispatch }, { folderUUID, level }) {
      return new Promise((resolve, reject) => {
        Promise.all([
          dispatch('getXml', `http://${state.vizGraphicHub.ip}/folders/${folderUUID}`),
          dispatch('getXml', `http://${state.vizGraphicHub.ip}/files/${folderUUID}`)
        ])
        .then(results => {
            const parentFolder = state.folders.find(f => f.UUID === folderUUID)
            const parentPath = level > 0 ? parentFolder.path : 'data'

            const folderEntries = results[0].querySelectorAll('entry')
            const filesEntries  = results[1].querySelectorAll('entry')
            
            let folders = []
            let files = []

            folderEntries.forEach(entry => {
              folders.push({
                ...state.defaultFolder,
                parentUUID: folderUUID,
                UUID: entry.querySelector('id').textContent.substring(9),
                level: level+1,
                title: entry.querySelector('title').textContent,
                category: entry.querySelector('category').attributes['term'].value,
                path: parentPath + '\\' + entry.querySelector('title').textContent,
              })
            })

            folders.sort((a, b) => a.title > b.title ? 1 : -1)

            filesEntries.forEach(entry => {
              files.push({
                UUID: entry.querySelector('id').textContent.substring(9),
                level: level+1,
                title: entry.querySelector('title').textContent,
                categories: Array.from(entry.querySelectorAll('category')).map(category => category.attributes['term'].value),
                thumbnail: undefined,
                path: parentPath + '\\' + entry.querySelector('title').textContent,
                updated: entry.querySelector('updated').textContent,
                thumbnailUrl: entry.getElementsByTagName('media:thumbnail')[0].attributes['url'].value,
              })
            })

            files.sort((a, b) => a.title > b.title ? 1 : -1)

            resolve({ folders, files })
        })
        .catch(error => {
          console.error('ERROR =', error)
          reject(error)
        })
      })
    },
    async loadRootFolder ({ state, commit, dispatch }) {
      try {
        const rootXmlData = await dispatch('getXml', `http://${state.vizGraphicHub.ip}/folders`)

        let rootFolder = {
          ...state.defaultFolder,
          UUID: rootXmlData.querySelector('entry').querySelector('id').textContent.substring(9),
          level: 0,
          path: rootXmlData.querySelector('entry').querySelector('title').textContent,
          title: rootXmlData.querySelector('entry').querySelector('title').textContent,
          category: rootXmlData.querySelector('entry').querySelector('category').attributes['term'].value,
        }

        commit('setFolders', [rootFolder])
        
        const folderInfo = await dispatch('getFolderInfo', { folderUUID: rootFolder.UUID, level: rootFolder.level } )
        rootFolder = { 
          ...rootFolder,
          ...folderInfo,
          isInfoLoaded: true,
        }

        commit('setFolders', [rootFolder])
      } catch (error) {
        console.error('ERROR =', error)
      }
    },
    async expandFolder ({ state, commit, dispatch }, folderUUID) {
      let folderIndex = state.folders.findIndex(f => f.UUID === folderUUID)
      let folder = state.folders[folderIndex]
      if (!folder.isInfoLoaded) {
        const folderInfo = await dispatch('getFolderInfo', { folderUUID: folder.UUID, level: folder.level })
        commit('updateFolderInfo', { folderUUID, folderInfo })
      }
      if (!folder.isExpanded) {
        commit('expandFolder', folderUUID)
      }
    },
    async setCurrentFolder({ state, commit, dispatch }, folderUUID) {
      let folderIndex = state.folders.findIndex(f => f.UUID === folderUUID)
      let folder = state.folders[folderIndex]

      if (!folder.isInfoLoaded) {
        const folderInfo = await dispatch('getFolderInfo', { folderUUID, level: folder.level })
        commit('updateFolderInfo', { folderUUID, folderInfo })
      }

      commit('setCurrentFolderPath', folder.path)
      commit('setFiles', state.folders[folderIndex].files)
      commit('setCurrentFolderIndex', folderIndex)
      await dispatch('loadThumbnails')
    },
    async openFolderByPath({ state, dispatch }, newPath) {
      // if (newPath === state.currentFolderPath) { return }
      const arrPath = newPath.replace(/\s/, '').split('\\').filter(s => s.trim() !== '')

      let folder = state.folders.find(f => f.title === arrPath[0])
      await dispatch('expandFolder', folder.UUID)
      arrPath.splice(0, 1)


      for (const pathPart of arrPath) {
        let folderWithThatPathPart = folder.folders.find(f => f.title === pathPart)
        if (folderWithThatPathPart) {
          let UUID = folderWithThatPathPart.UUID
          await dispatch('expandFolder', UUID)
          folder = state.folders.find(f => f.UUID === UUID)
        } else {
          break
        }
      }


      if (folder?.UUID) {
        dispatch('setCurrentFolder', folder.UUID)
      }
    },
    async loadThumbnails({ state }) {
      
      state.files.forEach(async file => {
        let response

        if (file.thumbnailUrl) {
          response = await fetch(file.thumbnailUrl, {
            method: 'GET',
            headers: {
              'Authorization': 'Basic ' + btoa(state.vizGhUser + ":" + state.vizGhPassword)
            }
          })
        } else {
          response = await fetch(`http://${state.vizGraphicHub.ip}/thumbnail/${file.UUID}?size=large`, {
            method: 'GET',
            headers: {
              'Authorization': 'Basic ' + btoa(state.vizGhUser + ":" + state.vizGhPassword)
            }
          })
          if (!response.ok) {
            response = await fetch(`http://${state.vizGraphicHub.ip}/thumbnail/${file.UUID}`, {
              method: 'GET',
              headers: {
                'Authorization': 'Basic ' + btoa(state.vizGhUser + ":" + state.vizGhPassword)
              }
            })
          }
        }
        try {
          const thumbnailBlob = await response.arrayBuffer()
          file.thumbnail = 'data:image/jpeg;base64,' + btoa(
            new Uint8Array(thumbnailBlob)
            .reduce((data, byte) => data + String.fromCharCode(byte), '')
          )
        } catch (error) {
          console.error('ERROR =', error)
        }
      })
      
    },
  },
}
