import { findMaxId } from './common'

export default {
  state: {
    jsonServer: {},
    scenarios: [],
    saveTimerId: {},
  },
  getters: {
    getScenario: (state) => (scenarioId) => {
      return state.scenarios.find(scenario => scenario.id === scenarioId)
    },
    scenariosCount: state => state.scenarios.length
  },
  mutations: {
    createJsonServer (state, newJsonServerIp) {
      state.jsonServer = {
        ip: newJsonServerIp,
        status: 'unknown',
      }
    },
    setJsonServerStatus (state, newStatus) {
      state.jsonServer.staus = newStatus
    },
    sortSteps (state, scenarioId) {
      const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
      if (scenario) {
        scenario.steps.sort((step1, step2) => step1.order - step2.order)
        let order = 1
        scenario.steps.forEach(step => step.order = order++)
      }
    },
    createNewStep (state, { scenarioId, stepName, countries }) {
      // I put createNewStep in actions because I use getters
      const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
      if (scenario) {
        let newStep = {}
        switch(stepName){
          case 'countries':
            newStep = JSON.parse(JSON.stringify(this.state.settings.defaultCountriesStep))
            countries.forEach(country => {
              newStep.countries.push({ 
                ...this.state.settings.defaultCountry,
                name:     country.name,
                croatian: country.croatian
              })
            })
            break
          case 'points':
            // deep clone of Default Point Step:
            newStep = JSON.parse(JSON.stringify(this.state.settings.defaultPointsStep))
            break
          case 'reset':
            newStep = { ...this.state.settings.defaultResetStep }
            break
        }
        newStep['transition'] = { ...this.state.settings.defaultTransition }
        newStep.order = scenario.steps.length + 1
        newStep.id = findMaxId(scenario.steps) + 1
        scenario.steps.push(newStep)
      }
    },
    moveStep (state, { scenarioId, stepId, shift }) {
      const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
      if (scenario) {
        const step = scenario.steps.find(step => step.id === stepId)
        if (step) {
          const oldOrder = step.order
          const newOrder = step.order + shift
          const anotherStep = scenario.steps.find(step => step.order === newOrder)
          if (anotherStep) {
            step.order = newOrder
            anotherStep.order = oldOrder
          }
        }
      }
    },
    copyStep (state, { scenarioId, stepId }) {
      const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
      if (scenario) {
        const step = scenario.steps.find(step => step.id === stepId)
        if (step) {
          const newStep = {
            ...JSON.parse(JSON.stringify(step)), // make a "deep copy" of step
            id: findMaxId(scenario.steps) + 1,
            order: scenario.steps.length + 1,
          }
          scenario.steps.push(newStep)
        }
      }
    },
    deleteStep (state, { scenarioId, stepId }) {
      const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
      if (scenario) {
        scenario.steps.splice( scenario.steps.findIndex(step => step.id === stepId) , 1)
      }
    },

    addCountryInStep (state , { scenarioId, stepId, country }) {
      const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
      if (scenario) {
        const step = scenario.steps.find(step => step.id === stepId)
        if (step) {
          step.countries.push({
            ...this.state.settings.defaultCountry,
            name: country.name,
            croatian: country.croatian
          })
        }
      }
    },
    deleteCountryInStep (state, { scenarioId, stepId, countryName }) {
      const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
      if (scenario) {
        const step = scenario.steps.find(step => step.id === stepId)
        if (step) {
          const countryIndex = step.countries.findIndex(country => country.name === countryName)
          step.countries.splice( countryIndex, 1)
        }
      }
    },

    addPoint (state, { scenarioId, stepId }) {
      const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
      if (scenario) {
        const step = scenario.steps.find(step => step.id === stepId)
        if (step) {
          step.points.push({ ...this.state.settings.defaultPoint, id: findMaxId(step.points)+1 })
        }
      }
    },
    addGeomPoint (state, { scenarioId, stepId, geom }) {
      const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
      if (scenario) {
        const step = scenario.steps.find(step => step.id === stepId)
        if (step && geom) {
          let path = geom.path
          if (geom.path?.startsWith('data\\')) path = path.substring(5)
          step.points.push({ 
            ...this.state.settings.defaultGeomPoint, 
            id: findMaxId(step.points)+1,
            path,
            thumbnailUrl: geom.thumbnailUrl,
          })
        }
      }
    },
    deletePoint (state, { scenarioId, stepId, pointId }) {
      const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
      if (scenario) {
        const step = scenario.steps.find(step => step.id === stepId)
        if (step) {
          const pointIndex = step.points.findIndex(point => point.id === pointId)
          step.points.splice( pointIndex, 1)
        }
      }
    },

    

    createScenarios (state, scenarios) {
      state.scenarios = scenarios
    },
  },
  actions: {
    async loadCountriesPresets ({ state, commit }) {
      const response = await fetch(`http://${state.jsonServer.ip}/presets`)
      const json = await response.json()

      commit('addCountriesPresets', json)
      commit('addCountriesPresetsIds', json)
    },

    async loadAllScenarios ({ state, commit }) {
      if (state.jsonServer.ip == undefined) {
        commit('setJsonServerStatus', 'Error')
        return
      }

      try {
        const response = await fetch(`http://${state.jsonServer.ip}/scenarios`)
        const json = await response.json()

        commit('createScenarios', json)
        commit('setJsonServerStatus', 'OK')
      } catch (error) { 
        console.error('ERROR when load JSON:', error)
        commit('setJsonServerStatus', 'Error')
      }
    },
    async loadScenario ({ state, commit, dispatch }, scenarioId) {
      if (state.jsonServer.ip == undefined) return

      try {
        const response = await fetch(`http://${state.jsonServer.ip}/scenarios/${scenarioId}`)
        const json = await response.json()

        dispatch('updateScenario', json)
        commit('sortSteps', scenarioId)
        commit('setJsonServerStatus', 'OK')
      } catch (error) { 
        console.error('ERROR when load JSON:', error)
        commit('setJsonServerStatus', 'Error')
      }
    },
    saveScenarioToServer ({ state }, scenarioId) {
      // save scenario with little delay
      clearTimeout(state.saveTimerId[scenarioId])
      state.saveTimerId[scenarioId] = setTimeout(() => {
        console.log("TRY TO SAVE:")
        const scenario = state.scenarios.find(scenario => scenario.id === scenarioId)
        if (scenario) {
          fetch(`http://${state.jsonServer.ip}/scenarios/${scenario.id}`, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            body: JSON.stringify(scenario)
          })
            .then(response => {
              if (response.ok) {
                console.log("SAVE: OK")
                state.jsonServer.status = 'OK'
              } else {
                console.log("SAVE: ERROR: ", response.statusText)
                state.jsonServer.status = 'ERROR'
              }
            })
            .catch(function(error) {  
              console.log('Fetch Error :-S', error)
            })
        }
      }, 1000)
    },
    async createNewScenario ({ state, commit }) {
      // const maxId = findMaxId(state.scenarios)
      try {
        const response = await fetch(`http://${state.jsonServer.ip}/scenarios`, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json;charset=utf-8' },
          body: JSON.stringify({
            ...this.state.settings.defaultScenario, 
            created_at: new Date().toLocaleString(),
            updated_at: new Date().toLocaleString(),
          })
        })
        if (response.ok) {
          const responseBody = await response.json()
          state.scenarios.push(responseBody)
        }
      } catch (error) { 
        console.error('ERROR when create a new Scenario:', error)
        commit('setJsonServerStatus', 'Error')
      }
    },
    updateScenario ({ state, dispatch }, newScenario) {
      if (typeof newScenario == 'undefined') return
      
      const index = state.scenarios.findIndex(scenario => scenario.id === newScenario.id)
      if (index >= 0) {
        state.scenarios[index] = newScenario
        state.scenarios[index].updated_at = new Date().toLocaleString()
      } else {
        state.scenarios.push(newScenario)
      }
      dispatch('saveScenarioToServer', newScenario.id)
    },
    async deleteScenario ({ state, commit, dispatch }, scenarioId) {
      try{
        const response = await fetch(`http://${state.jsonServer.ip}/scenarios/${scenarioId}`, {
          method: 'DELETE',
        })
        if (response.ok) {
          dispatch('loadAllScenarios')
        }
      } catch (error) { 
        console.error('ERROR when deleting the Scenario:', scenarioId, error)
        commit('setJsonServerStatus', 'Error')
      }
    },
  },
}
