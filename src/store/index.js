import { createStore } from 'vuex'
// import { createLogger } from 'vuex'
import scenarios from './scenarios'
import settings from './settings'
import viz from './viz'
import gh from './gh'

export const store = createStore({
  modules: {
    scenarios,
    settings,
    viz,
    gh,
  },
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  // plugins: [createLogger()],
})
