export default {
  state: {
    vizEngines: [],
  },
  mutations: {
    createVizEngines (state, newVizEnginesIps) {
      const vizEngines = []
      if (!Array.isArray(newVizEnginesIps)) { newVizEnginesIps = [newVizEnginesIps] }
      newVizEnginesIps.forEach(ip => {
        vizEngines.push({
          ip,
          status: 'unknown',
        })
      })
      state.vizEngines = vizEngines
    },
    setVizEngineStatus (state, { ip, status }) {
      const vizEngine = state.vizEngines.find(engine => engine.ip === ip)
      vizEngine.status = status
    },
  },
}
