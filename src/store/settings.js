export default {
  state: {
    hslColors: {},
    hslGradients: {},
    hslRandomRange: 10,
    countriesList: [],
    countriesSets: [],
    countriesSetsIds: [],
    
    defaultScenario: {},
    defaultTransition: {},
    defaultCountry: {},
    defaultCountriesStep: {},
    defaultResetStep: {},
    defaultPointsStep: {},
    defaultPoint: {},
    defaultGeomPoint: {},
  },
  getters: {
    allColors (state) {
      return Object.keys(state.hslColors).map(key => {
        return state.hslColors[key];
      })
    }
  },
  mutations: {
    setSettings (state, settings) {
      state.countriesList        = settings.countriesList
      state.countriesSets        = settings.countriesSets
      state.hslColors            = settings.hslColors
      state.hslGradients         = settings.hslGradients
      state.hslRandomRange       = settings.hslRandomRange

      state.defaultScenario      = settings.defaultScenario
      state.defaultTransition    = settings.defaultTransition
      state.defaultCountry       = settings.defaultCountry
      state.defaultCountriesStep = settings.defaultCountriesStep
      state.defaultPointsStep    = settings.defaultPointsStep
      state.defaultResetStep     = settings.defaultResetStep
      state.defaultPoint         = settings.defaultPoint
      state.defaultGeomPoint     = settings.defaultGeomPoint
    },
    addCountriesPresets (state, additionalCountriesPresets) {
      additionalCountriesPresets.forEach(preset => {
        state.countriesSets[preset.name] = preset.countries
      })
    },
    addCountriesPresetsIds (state, additionalCountriesPresets) {
      additionalCountriesPresets.forEach(preset => {
        state.countriesSetsIds[preset.name] = preset.id
      })
    },
    addNewCountriesPreset (state, newCountriesPreset) {
      state.countriesSets[newCountriesPreset.name] = newCountriesPreset.countries
      state.countriesSetsIds[newCountriesPreset.name] = newCountriesPreset.id
    },
    deleteCountriesPreset (state, nameCountriesPreset) {
      delete state.countriesSets[nameCountriesPreset]
      delete state.countriesSetsIds[nameCountriesPreset]
    }
  },
}
