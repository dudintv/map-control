export function findMaxId(arr) {
  if (!Array.isArray(arr) || arr.length <= 0) return 0
  return Math.max(...arr.map(item => Number(item.id)))
}
