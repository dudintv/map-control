import { createRouter, createWebHashHistory } from 'vue-router'
import EmptyScenraio from '@/components/pages/EmptyScenario'
import ScenarioPage from '@/components/pages/ScenarioPage'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { path: '/', component: EmptyScenraio, },
    { path: '/:id', component: ScenarioPage, },
  ]
})

export default router;
